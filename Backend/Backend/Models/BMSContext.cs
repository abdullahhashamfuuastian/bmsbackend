﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

namespace Backend.Models
{
    public partial class BMSContext : DbContext
    {
        public virtual DbSet<AgreementType> AgreementType { get; set; }
        public virtual DbSet<CustomerDetails> CustomerDetails { get; set; }
        public virtual DbSet<Invoice> Invoice { get; set; }
        public virtual DbSet<Month> Month { get; set; }
        public virtual DbSet<PaymentMode> PaymentMode { get; set; }
        public virtual DbSet<PaymentTerms> PaymentTerms { get; set; }
        public virtual DbSet<PaymentType> PaymentType { get; set; }
        public virtual DbSet<ReceivedPayments> ReceivedPayments { get; set; }
        public virtual DbSet<Rent> Rent { get; set; }
        public virtual DbSet<Shop> Shop { get; set; }
        public virtual DbSet<ShopType> ShopType { get; set; }
        public virtual DbSet<User> User { get; set; }
        public virtual DbSet<UserType> UserType { get; set; }
        public virtual DbSet<VendorDetails> VendorDetails { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
#warning To protect potentially sensitive information in your connection string, you should move it out of source code. See http://go.microsoft.com/fwlink/?LinkId=723263 for guidance on storing connection strings.
                optionsBuilder.UseSqlServer(@"Data Source=TEK;Initial Catalog=BMS;user id=sa1;password=123qwe;integrated security=True;Trusted_Connection=True;Encrypt=Yes;TrustServerCertificate=Yes");
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<AgreementType>(entity =>
            {
                entity.Property(e => e.AgreementName).IsUnicode(false);
            });

            modelBuilder.Entity<CustomerDetails>(entity =>
            {
                entity.Property(e => e.BankDetails).IsUnicode(false);

                entity.Property(e => e.BillTo).IsUnicode(false);

                entity.Property(e => e.Business).IsUnicode(false);

                entity.Property(e => e.Code).IsUnicode(false);

                entity.Property(e => e.ContactPerson).IsUnicode(false);

                entity.Property(e => e.Email).IsUnicode(false);

                entity.Property(e => e.Facebook).IsUnicode(false);

                entity.Property(e => e.Individual).IsUnicode(false);

                entity.Property(e => e.Instagram).IsUnicode(false);

                entity.Property(e => e.Linkedin).IsUnicode(false);

                entity.Property(e => e.Phone).IsUnicode(false);

                entity.Property(e => e.ShipTo).IsUnicode(false);

                entity.Property(e => e.ShopName).IsUnicode(false);

                entity.Property(e => e.TikTok).IsUnicode(false);

                entity.Property(e => e.Twitter).IsUnicode(false);

                entity.Property(e => e.Website).IsUnicode(false);

                entity.HasOne(d => d.PaymentTermsNavigation)
                    .WithMany(p => p.CustomerDetails)
                    .HasForeignKey(d => d.PaymentTerms)
                    .HasConstraintName("FK_Customer_Details_Payment_terms");
            });

            modelBuilder.Entity<Invoice>(entity =>
            {
                entity.Property(e => e.Date).IsUnicode(false);

                entity.Property(e => e.DueDate).IsUnicode(false);

                entity.Property(e => e.Invoice1).IsUnicode(false);

                entity.Property(e => e.Particulars).IsUnicode(false);

                entity.Property(e => e.Status).IsUnicode(false);

                entity.HasOne(d => d.CustomerNameNavigation)
                    .WithMany(p => p.Invoice)
                    .HasForeignKey(d => d.CustomerName)
                    .HasConstraintName("FK_Invoice_Customer");
            });

            modelBuilder.Entity<Month>(entity =>
            {
                entity.Property(e => e.MonthName).IsUnicode(false);
            });

            modelBuilder.Entity<PaymentMode>(entity =>
            {
                entity.Property(e => e.Id).ValueGeneratedNever();

                entity.Property(e => e.PaymentMode1).IsUnicode(false);
            });

            modelBuilder.Entity<PaymentTerms>(entity =>
            {
                entity.Property(e => e.PaymentTerms1).IsUnicode(false);
            });

            modelBuilder.Entity<PaymentType>(entity =>
            {
                entity.Property(e => e.Id).ValueGeneratedNever();

                entity.Property(e => e.PaymentType1).IsUnicode(false);
            });

            modelBuilder.Entity<ReceivedPayments>(entity =>
            {
                entity.Property(e => e.PaymentNo).IsUnicode(false);

                entity.Property(e => e.ReceiptNo).IsUnicode(false);

                entity.HasOne(d => d.CustomerNameNavigation)
                    .WithMany(p => p.ReceivedPayments)
                    .HasForeignKey(d => d.CustomerName)
                    .HasConstraintName("FK_Received_Payments_CustomerName");

                entity.HasOne(d => d.InvoiceNoNavigation)
                    .WithMany(p => p.ReceivedPayments)
                    .HasForeignKey(d => d.InvoiceNo)
                    .HasConstraintName("FK_Received_Payments_Invoice");

                entity.HasOne(d => d.ModeNavigation)
                    .WithMany(p => p.ReceivedPayments)
                    .HasForeignKey(d => d.Mode)
                    .HasConstraintName("FK_Received_Payments_Mode");

                entity.HasOne(d => d.TypeNavigation)
                    .WithMany(p => p.ReceivedPayments)
                    .HasForeignKey(d => d.Type)
                    .HasConstraintName("FK_Received_Payments_Payment_Type");
            });

            modelBuilder.Entity<Rent>(entity =>
            {
                entity.Property(e => e.Date).IsUnicode(false);

                entity.Property(e => e.Year).IsUnicode(false);
            });

            modelBuilder.Entity<Shop>(entity =>
            {
                entity.Property(e => e.AgreementStartDate).IsUnicode(false);

                entity.Property(e => e.ShopLocation).IsUnicode(false);

                entity.Property(e => e.ShopName).IsUnicode(false);

                entity.Property(e => e.ShopSize).IsUnicode(false);
            });

            modelBuilder.Entity<ShopType>(entity =>
            {
                entity.Property(e => e.ShopType1).IsUnicode(false);
            });

            modelBuilder.Entity<User>(entity =>
            {
                entity.Property(e => e.Email).IsUnicode(false);

                entity.Property(e => e.Gender).IsUnicode(false);

                entity.Property(e => e.JoiningDate).IsUnicode(false);

                entity.Property(e => e.Name).IsUnicode(false);

                entity.Property(e => e.Password).IsUnicode(false);
            });

            modelBuilder.Entity<UserType>(entity =>
            {
                entity.Property(e => e.UserType1).IsUnicode(false);
            });

            modelBuilder.Entity<VendorDetails>(entity =>
            {
                entity.Property(e => e.BankDetails).IsUnicode(false);

                entity.Property(e => e.Business).IsUnicode(false);

                entity.Property(e => e.Code).IsUnicode(false);

                entity.Property(e => e.ContactPerson).IsUnicode(false);

                entity.Property(e => e.Email).IsUnicode(false);

                entity.Property(e => e.Individual).IsUnicode(false);

                entity.Property(e => e.PaymentTerms).IsUnicode(false);

                entity.Property(e => e.Phone).IsUnicode(false);

                entity.Property(e => e.Salutation).IsUnicode(false);

                entity.Property(e => e.VendorAddress).IsUnicode(false);

                entity.Property(e => e.VendorName).IsUnicode(false);

                entity.Property(e => e.Website).IsUnicode(false);
            });
        }
    }
}
