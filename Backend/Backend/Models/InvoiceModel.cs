﻿using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;

namespace Backend.Models
{
    public class InvoiceModel
    {
        public int Id { get; set; }
        [StringLength(50)]
        public string Date { get; set; }
        [Column("Invoice#")]
        [StringLength(50)]
        public string Invoice1 { get; set; }
        [Column("Customer_Name")]
        public int? CustomerName { get; set; }
        [StringLength(50)]

        public string Customer1 { get; set; }
        public string Status { get; set; }
        [StringLength(50)]
        public string DueDate { get; set; }
        public decimal? Amount { get; set; }
        public decimal? BalanceDue { get; set; }
        public int? Qty { get; set; }
        [Column("Unit_Price")]
        public decimal? UnitPrice { get; set; }
        [StringLength(50)]
        public string Particulars { get; set; }

        public string getbyinvoicebycustomer { get; set; }
    }
}
