﻿using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;


namespace Backend.Models
{
    public class ReceiptModel
    {
        public int Id { get; set; }
        [StringLength(50)]
        public string Date { get; set; }
        [StringLength(50)]
        public string ReceiptNo { get; set; }
        [StringLength(50)]
        public string PaymentNo { get; set; }
        public int? Type { get; set; }

        public string type1 { get; set; }

        public string mode1 { get; set; }
        [Column("Reference_Number")]
        public int? ReferenceNumber { get; set; }
        [Column("Customer_Name")]
        public int? CustomerName { get; set; }

        public string customername1 { get; set; }    
        public int? InvoiceNo { get; set; }

        public string invoice1 { get; set; }
        public int? Mode { get; set; }
        public decimal? Amount { get; set; }
        [Column("Unused_Amount")]
        [MaxLength(50)]
        public byte[] UnusedAmount { get; set; }

        [ForeignKey("CustomerName")]
        [InverseProperty("ReceivedPayments")]
        public CustomerDetails CustomerNameNavigation { get; set; }
        [ForeignKey("InvoiceNo")]
        [InverseProperty("ReceivedPayments")]
        public Invoice InvoiceNoNavigation { get; set; }
        [ForeignKey("Mode")]
        [InverseProperty("ReceivedPayments")]
        public PaymentMode ModeNavigation { get; set; }
        [ForeignKey("Type")]
        [InverseProperty("ReceivedPayments")]
        public PaymentType TypeNavigation { get; set; }
    }
}
